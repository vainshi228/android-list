package com.example.list;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Layout;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.list.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Vector;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private Position pos = new Position(0,0);
    private Vector<String> itemList = new Vector<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        change_color(pos, pos);

        final Button buttonUp = findViewById(R.id.button_up);
        buttonUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UpKey(pos);
            }
        });
        final Button buttonLeft = findViewById(R.id.button_left);
        buttonLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LeftKey(pos);
            }
        });
        final Button buttonCenter = findViewById(R.id.button_center);
        buttonCenter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CenterKey(pos, itemList);
            }
        });
        final Button buttonRight = findViewById(R.id.button_right);
        buttonRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RightKey(pos);
            }
        });
        final Button buttonDown = findViewById(R.id.button_down);
        buttonDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DownKey(pos);
            }
        });
    }

    private void UpKey(Position pos){
        Position last_pos = new Position(pos.x, pos.y);
        if (pos.y == 0){
            pos.y = 4;
        }
        else pos.y -= 1;
        if (pos.y == 4 && pos.x > 8) pos.x = 8;
        change_color(last_pos, pos);
    }

    private void LeftKey(Position pos){
        Position last_pos = new Position(pos.x, pos.y);
        if (pos.x == 0){
            pos.x = 10;
        }
        else pos.x -= 1;
        if (pos.y == 4 && pos.x > 8) pos.x = 8;
        change_color(last_pos, pos);
    }

    private void RightKey(Position pos){
        Position last_pos = new Position(pos.x, pos.y);
        if (pos.x == 10){
            pos.x = 0;
        }
        else pos.x += 1;
        if (pos.y == 4 && pos.x > 8) pos.x = 8;
        change_color(last_pos, pos);
    }

    private void DownKey(Position pos){
        Position last_pos = new Position(pos.x, pos.y);
        if (pos.y == 4){
            pos.y = 0;
        }
        else pos.y += 1;
        if (pos.y == 4 && pos.x > 8) pos.x = 8;
        change_color(last_pos, pos);
    }

    private void CenterKey(Position pos, List<String> itemList){
        final TableLayout total_table = findViewById(R.id.total_table);
        final TextView element_keyboard = (TextView) ((TableRow) ((TableRow) total_table.getChildAt(pos.y)).getChildAt(pos.x)).getChildAt(0);
        final TextView textArea = findViewById(R.id.textArea);
        switch (element_keyboard.getText().toString()){
            case "Space":
                textArea.setText(textArea.getText().toString() + " ");
                break;
            case "Ent":
                if (textArea.getText().length() == 0) return;
                itemList.add(textArea.getText().toString());
                updateItemList(itemList);
                textArea.setText("");
                break;
            case "Back":
                final String text = textArea.getText().toString();
                if (text.length() == 0) return;
                textArea.setText(text.substring(0, text.length() - 1));
                break;
            case "Del":
                if (itemList.isEmpty()) return;
                itemList.remove(itemList.size() - 1);
                updateItemList(itemList);
                break;
            default:
                textArea.setText(textArea.getText().toString() + element_keyboard.getText().toString());
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_W:
            {
                UpKey(pos);
                return true;
            }
            case KeyEvent.KEYCODE_A:
            {
                LeftKey(pos);
                return true;
            }
            case KeyEvent.KEYCODE_S:
            {
                DownKey(pos);
                return true;
            }
            case KeyEvent.KEYCODE_D:
            {
                RightKey(pos);
                return true;
            }
            case KeyEvent.KEYCODE_L:
            {
                CenterKey(pos, itemList);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }


    private void change_color(Position last_position, Position new_position){

        final TableLayout total_table = findViewById(R.id.total_table);
        final TextView last_element =
                (TextView)
                    ((TableRow)
                        ((TableRow)
                            total_table.getChildAt(last_position.y)
                        ).getChildAt(last_position.x)
                    ).getChildAt(0);
        last_element.setBackgroundColor(0);
        final TextView cur_element = (TextView) ((TableRow) ((TableRow) total_table.getChildAt(new_position.y)).getChildAt(new_position.x)).getChildAt(0);
        cur_element.setBackgroundColor(getColor(R.color.red));
    }

    private void updateItemList(List<String> itemList){
        final LinearLayout itemListVue = findViewById(R.id.item_list);
        itemListVue.removeAllViews();
        for (String item: itemList) {
            final TextView itemVue = new TextView(this);
            itemVue.setText(item);
            itemListVue.addView(itemVue);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
    class Position{
        int x;
        int y;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}